package com.imooc.miaosha.rabbitMQ;

import com.imooc.miaosha.redis.RedisService;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MQsender {

    @Autowired
    AmqpTemplate amqpTemplate;



    public void send(Object message){
        String msg = RedisService.beanToString(message);
        amqpTemplate.convertAndSend(MQconfig.QUEUE,msg);
    }

    public void sendTopic(Object message){
        String msg = RedisService.beanToString(message);
        amqpTemplate.convertAndSend(MQconfig.TOPIC_EXCHANGE,"topic.key1",msg+"1");
    }
    public void sendMiaoshaMessage(MiaoshaMessage miaoshaMessage){
        String msg = RedisService.beanToString(miaoshaMessage);
        amqpTemplate.convertAndSend(MQconfig.MIAOSHA_QUEUE,msg);
    }

}
